BUATLAH SEBUAH PROGRAM DENGAN KETENTUAN SEBAGAI BERIKUT:
==
1.  Merupakan sistem informasi dengan CRUD standar (nama, pekerjaan, tanggal lahir)
2. Pada saat insert data, generate key di database dengan menggunakan format UUID
3. Siapkan data seeder untuk otomatis input ke dalam database (minimal 50)
4. Tampilkan data yang di input dan seed kedalam tabel (server side pagination)
5. Sediakan fitur pencarian dan filter data
6. Tampilan data dengan ketentuan sebagai berikut:
    - Jika tanggal yang di tampilkan merupakan tanggal genap maka di berikan keterangan "tanggal genap", jika ganjil maka "tanggal ganjil”
    - Jika minggu yang di tampilkan merupakan minggu genap maka di berikan keterangan "minggu genap", jika ganjil maka "minggu ganjil”
7. Buat sebuah payment page untuk pembelian produk dengan menggunakan redirect payment ipaymu (dokumentasi api bisa di lihat di https://ipaymu.com/api-collection)


Cara Menggunakan
===
1. sesuaikan koneksi DB di file .env 
2. Ketika perintah pada terminal ``php artisan migrate --seed``
3. crud user dapat diakses mengaktfikan server laravel ```php artisan serve``` dan defaultnya dapat diakses pada url ```localhost:8000/``` atau perhatikan log yang ada pada terminal port berapa project ini jalan, dan sesuaikan port jika tidak sesuai.

4. Untuk akses CRUD ```localhost:8000/```
4. Untuk akses Api iPaymu ```localhost:8000/ipaymu```