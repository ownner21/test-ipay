<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IpayController extends Controller
{

    public function index()
    {
        $body['account'] = '1179000899';

        $header = $this->header($body);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://sandbox.ipaymu.com/api/v2/balance',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($header['body']),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'signature: '.$header['signature'],
            'va: '.$header['va'],
            'timestamp: '.$header['timestamp']
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $res= json_decode($response,true);

        return view('ipay.index', compact('res'));
    }
    public function client()
    {
        $transactions = DB::table('transactions')->simplePaginate();
        return view('ipay.client', compact('transactions'));
    }
    public function header($body)
    {

        $va           = '1179000899'; //get on iPaymu dashboard
        $secret       = 'QbGcoO0Qds9sQFDmY0MWg1Tq.xtuh1'; //get on iPaymu dashboard

        $method       = 'POST'; //method

        //Request Body//
        $body['returnUrl']  = 'http://test.kampungpedia.com/ipay/callback';
        $body['cancelUrl']  = 'http://test.kampungpedia.com/ipay/callback/cancel';
        $body['notifyUrl']  = 'http://test.kampungpedia.com/ipay/callback/notif';
        //End Request Body//

        //Generate Signature
        // *Don't change this
        $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
        $requestBody  = strtolower(hash('sha256', $jsonBody));
        $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
        $signature    = hash_hmac('sha256', $stringToSign, $secret);
        $timestamp    = Date('YmdHis');
        //End Generate Signature

        return [
            'signature' => $signature,
            'timestamp' => $timestamp,
            'va' => $va,
            'body' => $body
        ];

    }

    public function redirecPayment(Request $request)
    {
        //Request Body//
        $body['product']    = array($request->product);
        $body['qty']        = array($request->qty);
        $body['price']      = array($request->price);
        //End Request Body//

        $header = $this->header($body);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://sandbox.ipaymu.com/api/v2/payment',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($header['body']),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'signature: '.$header['signature'],
            'va: '.$header['va'],
            'timestamp: '.$header['timestamp']
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $res= json_decode($response,true);
        if (isset($res['Data'])) {
            return redirect()->away($res['Data']['Url']);
        }else{
            return response()->json($res, 200);
        }

        // return response()->json(json_decode($response,true), 200);
    }

    public function create()
    {
        return view('ipay.create');
    }

    public function directPayment(Request $request)
    {
        $product = $request->product;
        $qty = $request->qty;
        $price = $request->price;
        $weight = $request->weight;
        $width = $request->width;
        $height = $request->height;
        $length = $request->length;
        
        $total = array_map(function($x, $y) { return $x * $y; }, $price, $qty);
        
        $body = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'amount' => array_sum($total),
            'expired' => '24',
            'expiredType' => 'hours',
            'comments' => $request->comments,
            'referenceId' => 'IP'.rand(100000,999999),
            'paymentMethod' => $request->paymentMethod,
            'paymentChannel' => $request->paymentMethod,
            'product' => $product,
            'qty' => $qty,
            'price' => $price,
            'weight' => $weight,
            'width' => $width,
            'height' => $height,
            'length' => $length,
            'deliveryArea' => $request->deliveryArea,
            'deliveryAddress' => $request->deliveryAddress
        ];

        $header = $this->header($body);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://sandbox.ipaymu.com/api/v2/payment/direct',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($header['body']),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'signature: '.$header['signature'],
            'va: '.$header['va'],
            'timestamp: '.$header['timestamp']
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $res= json_decode($response,true);
        if (isset($res['Data'])) {
            return redirect()->away($res['Data']['QrTemplate']);
        }else{
            return response()->json($res, 200);
        }
    }

    public function history(Request $request)
    {

        if(!isset($_GET['page'])){
            $page= 1;
        }elseif($_GET['page']==""){
            $page= 1;
        }else{
            $page= $_GET['page'];
        }

        $startDate =$request->has('start_date') ? $request->start_date : date('Y-m-d',  strtotime("-1 month"));
        $endDate = $request->has('end_date') ? $request->end_date : date('Y-m-d');

        $body = array('startdate' => $startDate,'enddate' => $endDate,'page' => $page);

        $header = $this->header($body);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://sandbox.ipaymu.com/api/v2/history',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($header['body']),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'signature: '.$header['signature'],
            'va: '.$header['va'],
            'timestamp: '.$header['timestamp']
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $res= json_decode($response,true);
        if ($res['Status'] == 200) {
            $datas = $res['Data']['Transaction'];
        }else{
            $datas = array();
        }

        return view('ipay.history', compact('startDate', 'endDate', 'datas','page'));
    }

    public function check($id)
    {
        $body = ['transactionId' => $id];

        $header = $this->header($body);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://sandbox.ipaymu.com/api/v2/transaction',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($header['body']),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'signature: '.$header['signature'],
            'va: '.$header['va'],
            'timestamp: '.$header['timestamp']
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $res= json_decode($response,true);
        if ($res['Status'] == 200) {
            $data = $res['Data'];
        }else{
            $data = array();
        }
        return view('ipay.check',compact('data'));
    }
}
