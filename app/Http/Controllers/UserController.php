<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

  
class UserController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'job' => 'required',
            'date_of_birt' => 'required|date_format:Y-m-d',
        ]);

        DB::table('users')->insert([
            'uuid'=> Str::uuid()->toString(), 
            'name' => $request->name,
            'job' => $request->job,
            'date_of_birt' => $request->date_of_birt,
        ]);

        return response()->json(['message'=> 'Berhasil Membuat User Baru','icon_alert' => 'success','title_alert' =>'Berhasil'], 200);
    }
    public function update(Request $request)
    {
        $this->validate($request,[
            'uuid' => 'required',
            'name' => 'required',
            'job' => 'required',
            'date_of_birt' => 'required|date_format:Y-m-d',
        ]);

        DB::table('users')->where('uuid', $request->uuid)->update([
            'name' => $request->name,
            'job' => $request->job,
            'date_of_birt' => $request->date_of_birt,
        ]);

        return response()->json(['message'=> 'Berhasil Update','icon_alert' => 'success','title_alert' =>'Berhasil'], 200);
    }
    public function delete(Request $request)
    {
        $del = DB::table('users')->where('id', $request->id)->delete();
        
        return response()->json(['error'=>true, 'message'=> 'Berhasil Delete User','icon_alert' => 'success','title_alert' =>'Berhasil'], 200);
    }
}
