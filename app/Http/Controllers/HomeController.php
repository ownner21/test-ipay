<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    public function data()
    {
        return DataTables::of(DB::table('users'))->toJson();
    }
    public function index()
    {
        // session()->flash('sweetstatus', "'Berhasil','Berhasil Menghapus Semua Data','success'");
        return view('welcome');
    }

    public function userData(Request $request)
    {
        $perPage =10;

        if(!isset($_GET['page'])){
            $page= 1;
        }elseif($_GET['page']==""){
            $page= 1;
        }else{
            $page= $_GET['page'];
        }

        $start    = ($page - 1) * $perPage;

        $dbRaw = [];
        if ($request->has('ddate')) {
            if ($request->ddate != '') {
                $dbRaw[] = $request->ddate == 'genap' ?  'date_of_birt % 2 = 0' :  'date_of_birt % 2 <> 0';
            }
        }
        if ($request->has('wdate')) {
            if ($request->wdate != '') {
                $dbRaw[] = $request->wdate == 'genap' ? 'WEEK(date_of_birt) % 2 = 0' : 'WEEK(date_of_birt) % 2 <> 0';
            }
        }

        $whereRaw = implode(' AND ', $dbRaw);

        if($whereRaw != ''){
            $users = DB::table('users')->select('*',DB::raw('WEEK(date_of_birt) as week'))->whereRaw($whereRaw)->limit($perPage)->offset($start)->get();
        
        }else{
            $users = DB::table('users')->limit($perPage)->offset($start)->orderBy('id', 'DESC')->get();
        }
        return response()->json(['data' => $users, 'per_page' => $perPage], 200);
    }
    //
}
