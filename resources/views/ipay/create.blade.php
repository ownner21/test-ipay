@extends('template')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            
            <a href="{{route('ipay')}}" class="mt-3 float-right btn btn-sm btn-secondary">ipaymu home</a>
            <h3 class="mt-3">Create Direct Payment</h3>
            <hr>

            <br>
            <form action="{{route('ipay.direct')}}" target="_blank"  method="get">
                <b>Payment</b><hr>
                <div class="form-group row">
                    <label for="paymentMethod" class="col-sm-3 col-form-label">Payment Method</label>
                    <div class="col-sm-3">
                        <select name="paymentMethod" id="paymentMethod" class="form-control">
                            <option value="qris">Qris</option>
                        </select>
                    </div>
                    <label for="paymentChannel" class="col-sm-3 col-form-label">Payment Channel</label>
                    <div class="col-sm-3">
                        <select name="paymentChannel" id="paymentChannel" class="form-control">
                            <option value="qris">Qris</option>
                        </select>
                    </div>
                </div>
                <br>
                <b>Buyer</b><hr>
                <div class="form-group row">
                    <label for="name" class="col-sm-3 col-form-label">Nama</label>
                    <div class="col-sm-3">
                        <input type="text" name="name" id="name" value="syaifudin" class="form-control">
                    </div>
                    <label for="phone" class="col-sm-3 col-form-label">Phone</label>
                    <div class="col-sm-3">
                        <input type="text" name="phone" id="phone" value="081999501092" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-3">
                        <input type="email" name="email" id="email" value="syaifudin@email.com" class="form-control">
                    </div>
                    <label for="comments" class="col-sm-3 col-form-label">Catatan</label>
                    <div class="col-sm-3">
                        <input type="text" name="comments" id="comments" value="Catatan" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="deliveryArea" class="col-sm-3 col-form-label">Delivery Area</label>
                    <div class="col-sm-3">
                        <input type="text" name="deliveryArea" id="deliveryArea" value="76111" class="form-control">
                    </div>
                    <label for="deliveryAddress" class="col-sm-3 col-form-label">Delivery Address</label>
                    <div class="col-sm-3">
                        <input type="text" name="deliveryAddress" id="deliveryAddress" value="Denpasar" class="form-control">
                    </div>
                </div>

                <br><br>
                <table class="table table-sm">
                    <tr>
                        <th>Product</th><th>Qty</th><th>Price</th><th>Weight</th><th>Widht</th><th>Height</th><th>Length</th>
                    </tr>
                    <tr>
                        <td><input type="text" name="product[]" class="form-control" placeholder="Product" value="Produk 1"></td>
                        <td><input type="number" name="qty[]" class="form-control" placeholder="Qty" min="1" value="1"></td>
                        <td><input type="number" name="price[]" class="form-control" placeholder="Price" value="10000"></td>
                        <td><input type="number" name="weight[]" class="form-control" placeholder="Weight" value="1"></td>
                        <td><input type="number" name="width[]" class="form-control" placeholder="Width" value="1"></td>
                        <td><input type="number" name="height[]" class="form-control" placeholder="Height" value="1"></td>
                        <td><input type="number" name="length[]" class="form-control" placeholder="Length" value="1"></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="product[]" class="form-control" placeholder="Product" value="Produk 2"></td>
                        <td><input type="number" name="qty[]" class="form-control" placeholder="Qty" min="1" value="1"></td>
                        <td><input type="number" name="price[]" class="form-control" placeholder="Price" value="10000"></td>
                        <td><input type="number" name="weight[]" class="form-control" placeholder="Weight" value="1"></td>
                        <td><input type="number" name="width[]" class="form-control" placeholder="Width" value="1"></td>
                        <td><input type="number" name="height[]" class="form-control" placeholder="Height" value="1"></td>
                        <td><input type="number" name="length[]" class="form-control" placeholder="Length" value="1"></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="product[]" class="form-control" placeholder="Product" value="Produk 3"></td>
                        <td><input type="number" name="qty[]" class="form-control" placeholder="Qty" min="1" value="1"></td>
                        <td><input type="number" name="price[]" class="form-control" placeholder="Price" value="10000"></td>
                        <td><input type="number" name="weight[]" class="form-control" placeholder="Weight" value="1"></td>
                        <td><input type="number" name="width[]" class="form-control" placeholder="Width" value="1"></td>
                        <td><input type="number" name="height[]" class="form-control" placeholder="Height" value="1"></td>
                        <td><input type="number" name="length[]" class="form-control" placeholder="Length" value="1"></td>
                    </tr>
                    
                </table>
                <div class="form-group row mt-4">
                    <button type="submit" class="btn btn-block btn-success">Create</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

            {{-- 'amount' => '10000',
            'expired' => '24',
            'expiredType' => 'hours',
            'referenceId' => '1',
            'product[]' => 'produk 1',
            'qty[]' => '1',
            'price[]' => '10000',
            'weight[]' => '1',
            'width[]' => '1',
            'height[]' => '1',
            'length[]' => '1', --}}
