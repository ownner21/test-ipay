@extends('template')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            
            <h3 class="mt-3">Transaction Client</h3>
            <hr>

            <table class="table table-sm">
                <tr>
                    <th>Product</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                @foreach ($transactions as $transaction)
                    <tr>
                        <td>{{$transaction->product}}</td>
                        <td>{{$transaction->qty}}</td>
                        <td>{{$transaction->price}}</td>
                        <td>{{$transaction->status}}</td>
                        <td>
                            <a href="{{route('ipay.redirect',['product' => $transaction->product,'qty'=> $transaction->qty, 'price'=>$transaction->price])}}" target="_blank" class="btn btn-sm btn-outline-success">Pay</a>
                        </td>
                    </tr>
                @endforeach
            </table>

            {{$transactions->links()}}
        </div>
    </div>
</div>
@endsection
