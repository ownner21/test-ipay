@extends('template')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            
            <a href="{{route('ipay.history')}}" class="mt-3 float-right btn btn-sm btn-secondary">History</a>
            <h3 class="mt-3 mb-3">iPaymu Check</h3>

          <hr>


          @if (isset($data['TransactionId']))
          <table class="table table-sm">
            <tr><td>TransactionId</td><th>{{$data['TransactionId']}}</th></tr>
            <tr><td>SessionId</td><th>{{$data['SessionId']}}</th></tr>
            <tr><td>ReferenceId</td><th>{{$data['ReferenceId']}}</th></tr>
            <tr><td>RelatedId</td><th>{{$data['RelatedId']}}</th></tr>
            <tr><td>Sender</td><th>{{$data['Sender']}}</th></tr>
            <tr><td>Receiver</td><th>{{$data['Receiver']}}</th></tr>
            <tr><td>Amount</td><th>{{$data['Amount']}}</th></tr>
            <tr><td>Fee</td><th>{{$data['Fee']}}</th></tr>
            <tr><td>Status</td><th>{{$data['Status']}}</th></tr>
            <tr><td>StatusDesc</td><th>{{$data['StatusDesc']}}</th></tr>
            <tr><td>Type</td><th>{{$data['Type']}}</th></tr>
            <tr><td>TypeDesc</td><th>{{$data['TypeDesc']}}</th></tr>
            <tr><td>Notes</td><th>{{$data['Notes']}}</th></tr>
            <tr><td>CreatedDate</td><th>{{$data['CreatedDate']}}</th></tr>
            <tr><td>ExpiredDate</td><th>{{$data['ExpiredDate']}}</th></tr>
            <tr><td>SuccessDate</td><th>{{$data['SuccessDate']}}</th></tr>
            <tr><td>SettlementDate</td><th>{{$data['SettlementDate']}}</th></tr>
            <tr><td>PaymentChannel</td><th>{{$data['PaymentChannel']}}</th></tr>
            <tr><td>PaymentCode</td><th>{{$data['PaymentCode']}}</th></tr>
            <tr><td>BuyerName</td><th>{{$data['BuyerName']}}</th></tr>
            <tr><td>BuyerPhone</td><th>{{$data['BuyerPhone']}}</th></tr>
            <tr><td>BuyerEmail</td><th>{{$data['BuyerEmail']}}</th></tr>
          </table>
          @else
          <div class="text-center text-secondary">ID Transaksi Tidak Ditemukan</div>
          @endif

        </div>
    </div>
</div>
@endsection
