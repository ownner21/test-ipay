@extends('template')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            
            <a href="{{route('index')}}" class="mt-3 float-right btn btn-sm btn-light">CRUD DOM JS</a>
            <h3 class="mt-3 mb-3">iPaymu</h3>

            <a href="{{route('ipay.history')}}" class="btn btn-primary btn-sm">History</a>
            <a href="{{route('ipay.create')}}" class="btn btn-success btn-sm">Buat Direct Payment</a>
            <a href="{{route('ipay.client')}}" class="btn btn-warning btn-sm">Redirect Payment V.Client</a>
            <br><br>
            @if (isset($res['Data']))
            <table class="table table-sm">
                <tr><td>Account</td><th>{{$res['Data']['Va']}}</th></tr>
                <tr><td>Merchant Balance</td><th>{{$res['Data']['MerchantBalance']}}</th></tr>
                <tr><td>Member Balance</td><th>{{$res['Data']['MemberBalance']}}</th></tr>
            </table>
            @endif

            <br><br><br>
        </div>
    </div>
</div>
@endsection
