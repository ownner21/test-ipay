@extends('template')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            
            <a href="{{route('ipay')}}" class="mt-3 float-right btn btn-sm btn-secondary">ipaymu home</a>
            <h3 class="mt-3 mb-3">iPaymu History</h3>

          <hr>

            <form action="{{url()->current()}}" method="get">
                <button class="btn btn-primary float-right" type="submit">Filter</button>
                <div class="form-group row">
                    <label for="startdate" class="col-sm-2 col-form-label">Tanggal Mulai</label>
                    <div class="col-sm-2">
                        <input type="date" name="start_date" id="startdate" value="{{$startDate}}" class="form-control">
                    </div>
                    <label for="enddate" class="col-sm-2 col-form-label">Tanggal Selesai</label>
                    <div class="col-sm-2">
                        <input type="date" name="end_date" id="enddate" value="{{$endDate}}" class="form-control">
                    </div>
                    <label for="page" class="text-right col-sm-2 col-form-label">Page</label>
                    <div class="col-sm-2">
                        <input type="number" name="page" id="page" value="{{$page}}" class="form-control">
                    </div>
                </div>
            </form>

            <br>
          
          <table class="table table-sm text-center">
            <tr>
                <th>TransactionID</th>
                <th>Status</th>
                <th>Type</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
            @foreach ($datas as $data)
                <tr>
                    <td>{{$data['TransactionId']}}</td>
                    <td>{{$data['StatusDesc']}}</td>
                    <td>{{$data['TypeDesc']}}</td>
                    <td>{{$data['CreatedDate']}}</td>
                    <td>
                        <a href="{{route('ipay.check',['id'=>$data['TransactionId']])}}" class="btn btn-sm btn-success">Check</a>
                    </td>
                </tr>
            @endforeach
          </table>

        </div>
    </div>
</div>
@endsection
