<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            setTimeout(function() {
                window.close();
                }, 3000);
        });
    </script>
</head>
<body>
    {{-- sarat untuk bisa otomatis close harus permintaan pembuatan tab baru dilakukan dicoding bukan. 
    contoh 
    function openWindow()
    {
        var phone = $('#hp').val();
        console.log(phone);
        window.open( "login/"+phone );
    }

    tutorial 
    https://www.thesitewizard.com/javascripts/close-browser-tab-or-window.shtml --}}
    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @if(Session::has('sweetstatus'))
    <script>
        Swal.fire({!!Session::get('sweetstatus')!!});
    </script>
    @endif
</body>
</html>