@extends('template')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            
            <a href="{{route('ipay')}}" class="mt-3 float-right btn btn-sm btn-primary">ipaymu home</a>
            <h3 class="mt-3">DOM Js CRUD</h3>
            <hr>

            <button class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-form" data-title="Tambah User" data-ket="tambah">Tambah User</button>
            
            
            <form action="{{url()->current()}}" method="get">
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-3 col-form-label">Filter Tanggal</label>
                    <div class="col-sm-3">
                        <select onChange="dDate(this.value)" class="form-control">
                            <option value="">Semua</option>
                            <option value="ganjil" {{isset($_GET['ddate'])? ($_GET['ddate'] == 'ganjil') ? 'selected': '' : ''}}>Ganjil</option>
                            <option value="genap" {{isset($_GET['ddate'])? ($_GET['ddate'] == 'genap') ? 'selected': '' : ''}}>Genap</option>
                        </select>
                    </div>
                    <label for="staticEmail" class="col-sm-3 col-form-label">Filter Minggu</label>
                    <div class="col-sm-3">
                        <select onChange="wDate(this.value)" class="form-control">
                            <option value="">Semua</option>
                            <option value="ganjil" {{isset($_GET['wdate'])? ($_GET['wdate'] == 'ganjil') ? 'selected': '' : ''}}>Ganjil</option>
                            <option value="genap" {{isset($_GET['wdate'])? ($_GET['wdate'] == 'genap') ? 'selected': '' : ''}}>Genap</option>
                        </select>
                    </div>
                </div>
            </form>
            <table class="mt-4 table table-sm">
                <tr>
                    <th>Name</th>
                    <th>Job</th>
                    <th>Tgl Lahir</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Minggu</th>
                    <th class="text-center">Action</th>
                </tr>
                <tbody id="dataUser">
                </tbody>
            </table>
            <br><hr>

            <div id="paginateUser">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item"><a class="page-link" onClick="btnPrev()" id="btnPrev">Previous</a></li>
                        <li class="page-item"><a class="page-link" onClick="btnNext()" id="btnNext">Next</a></li>
                    </ul>
                </nav>
            </div>
            
        </div>
        {{-- <div class="col-12">
            <p>Server Side View Datatable <a href="{{route('serverside')}}" class="btn-link">Server Side</a></p>
        </div> --}}
    </div>
</div>



<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <input type="hidden" name="uuid" id="uuid" value="">
        
        <div class="modal-body">
          <div class="form-group row">
            <label class="col-sm-3 col-form-label">Name</label>
            <div class="col-sm-9">
              <input class="form-control " id="name" type="text" name="name">
              <div id="in-valid-name" class="invalid-feedback"></div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 col-form-label">Job</label>
            <div class="col-sm-9">
              <input class="form-control" id="job" type="text" name="job">
              <div id="in-valid-job" class="invalid-feedback"></div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 col-form-label">Date Of Birt</label>
            <div class="col-sm-9">
              <input class="form-control" id="date_of_birt" type="date" name="date_of_birt">
              <div id="in-valid-date_of_birt" class="invalid-feedback"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" onclick="actionSubmit()" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('script')

<script>
    var currenPage
    var maxPage = false
    var cPage = parseInt(location.search.split('page=')[1]);
    currenPage = !cPage ? 1 : cPage;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    reloadData()

    function setURLSearchParam(key, value) {
        const url = new URL(window.location.href);
        url.searchParams.set(key, value);
        window.history.pushState({ path: url.href }, '', url.href);
    }


    function btnNext() {
        var page = currenPage+1;
        setURLSearchParam('page',page)
        document.getElementById('btnPrev').classList.remove("disabled");
        if (maxPage) {
            document.getElementById('btnNext').classList.add("disabled");
        }else{
            reloadData();
            currenPage = page;
        }
    }
    
    function btnPrev() {
        var page = currenPage-1;
        setURLSearchParam('page',page)
        document.getElementById('btnNext').classList.remove("disabled");
        if(page == 0){
            document.getElementById('btnPrev').classList.add("disabled");
        }else{
            maxPage = false
            document.getElementById('btnPrev').classList.remove("disabled");
            reloadData();
            currenPage = page;
        }
    }

    function dDate(val) {
        setURLSearchParam('ddate',val)
        setURLSearchParam('page',1)
        reloadData()
    }
    function wDate(val) {
        setURLSearchParam('wdate',val)
        setURLSearchParam('page',1)
        reloadData()
    }

    function isInValid(idInput, idText, valueError) {
        document.getElementById(idInput).classList.add("is-invalid");
        document.getElementById(idText).innerHTML = valueError;
    }
    function isValid(idInput, idText) {
        document.getElementById(idInput).classList.remove("is-invalid");
        document.getElementById(idText).innerHTML = '';
    }
    
    function actionSubmit() {
        var uuid = document.getElementById("uuid").value;
        var name = document.getElementById("name").value;
        var job = document.getElementById("job").value;
        var date_of_birt = document.getElementById("date_of_birt").value;
        if (uuid == '') {
            console.log('tambah');
            $.ajax({ 
                type: 'POST', 
                url: "{{route('user.store')}}", 
                data: {
                    name : name,
                    job : job,
                    date_of_birt : date_of_birt,
                },
                dataType: 'json',
                success: function (data) { 
                    isValid('name','in-valid-name')
                    isValid('job','in-valid-job')
                    isValid('date_of_birt','in-valid-date_of_birt')

                    setURLSearchParam('page',1)
                    reloadData()
                    $('#modal-form').modal('hide');

                    Swal.fire(data.title_alert,data.message,data.icon_alert);
                },
                error: function(data){
                    var errors = data.responseJSON.errors 
                    if (errors.name) {  isInValid('name','in-valid-name',errors.name) }else{ isValid('name','in-valid-name')}
                    if (errors.job) {  isInValid('job','in-valid-job',errors.job) }else{ isValid('job','in-valid-job')}
                    if (errors.date_of_birt) {  isInValid('date_of_birt','in-valid-date_of_birt',errors.date_of_birt) }else{ isValid('date_of_birt','in-valid-date_of_birt')}
                }
            })
        }else{
            console.log('update');
            $.ajax({ 
                type: 'PUT', 
                url: "{{route('user.update')}}", 
                data: {
                    uuid : uuid,
                    name : name,
                    job : job,
                    date_of_birt : date_of_birt,
                },
                dataType: 'json',
                success: function (data) { 

                    isValid('name','in-valid-name')
                    isValid('job','in-valid-job')
                    isValid('date_of_birt','in-valid-date_of_birt')

                    setURLSearchParam('page',1)
                    reloadData()
                    $('#modal-form').modal('hide');

                    Swal.fire(data.title_alert,data.message,data.icon_alert);

                },
                error: function(data){
                    var errors = data.responseJSON.errors 
                    if (errors.name) {  isInValid('name','in-valid-name',errors.name) }else{ isValid('name','in-valid-name')}
                    if (errors.job) {  isInValid('job','in-valid-job',errors.job) }else{ isValid('job','in-valid-job')}
                    if (errors.date_of_birt) {  isInValid('date_of_birt','in-valid-date_of_birt',errors.date_of_birt) }else{ isValid('date_of_birt','in-valid-date_of_birt')}
                }
            })
        }


    }

function destroy(id) {
    Swal.fire({
            title: "Apakah Kamu Yakin?",
            text: "pesan",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Lanjutkan'
        })
        .then((willDelete) => {
            if (willDelete.isConfirmed) {
                $.ajax({
                    url: "{{route('user.delete')}}",
                    type: 'DELETE',
                    data: {
                        id : id
                    },
                    dataType: "JSON",
                    success: function (response) {
                        Swal.fire(response.title_alert,response.message,response.icon_alert);
                        $('#user'+id).remove();
                        reloadData()
                    },
                    error: function (xhr) {
                        console.log(xhr.responseText);
                    }
                });

            } else {
                Swal.fire("Batal untuk menghapus item");
            }
        });
}

    function reloadData() {
        $.ajax({ 
            type: 'GET', 
            url: "{{route('user.data')}}"+window.location.search, 
            dataType: 'json',
            success: function (data) { 
                $('#dataUser').empty()
                if (data.data.length < data.per_page) {
                    maxPage = true
                    // document.getElementById('btnNext').classList.remove("disabled");
                    // console.log('dibawah')
                }
                $.each(data.data, function(index, element) {
                    var birt = new Date(element.date_of_birt);
                    var dDate = birt.getDate() % 2 == 1 ? 'Ganjil' : 'Genap';

                    var wDate = element.week % 2 == 1 ? 'Ganjil' : 'Genap';
                    $('#dataUser').append('<tr id="user'+element.id+'"><td>'+element.name+'</td><td>'+element.job+'</td><td>'+element.date_of_birt+'</td><td class="text-center">'+dDate+'</td>><td class="text-center">'+wDate+'</td><td class="text-center"><button class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#modal-form" data-title="Edit User" data-ket="edit" data-uuid="'+element.uuid+'" data-name="'+element.name+'" data-job="'+element.job+'" data-date_of_birt="'+element.date_of_birt+'">Edit User</button><button class="btn btn-outline-danger btn-sm ml-2" onClick="destroy('+element.id+')">Hapus</button></td></tr>');

                });
            }
        });
    }


    $('#modal-form').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title') 
        var ket = button.data('ket')

        var modal = $(this)
        modal.find('.modal-title').text(title)

        if (ket == 'tambah') {
            modal.find('#uuid').val('')
            modal.find('#name').val('')
            modal.find('#job').val('')
            modal.find('#date_of_birt').val('')
        }else{
            var uuid = button.data('uuid') 
            var name = button.data('name') 
            var job = button.data('job') 
            var date_of_birt = button.data('date_of_birt') 

            modal.find('#uuid').val(uuid)
            modal.find('#name').val(name)
            modal.find('#job').val(job)
            modal.find('#date_of_birt').val(date_of_birt)

        }
    })

</script>
@endsection