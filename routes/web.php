<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');
Route::get('/user', 'HomeController@index')->name('user');
Route::post('/user', 'UserController@store')->name('user.store');
Route::put('/user', 'UserController@update')->name('user.update');
Route::delete('/userdel', 'UserController@delete')->name('user.delete');
Route::get('/data', 'HomeController@data')->name('data');

Route::get('/userData', 'HomeController@userData')->name('user.data');

Route::prefix('ipaymu')->group(function () {
    Route::get('', 'IpayController@index')->name('ipay');
    Route::get('create', 'IpayController@create')->name('ipay.create');
    Route::get('client', 'IpayController@client')->name('ipay.client');
    Route::get('history', 'IpayController@history')->name('ipay.history');
    Route::get('direct-payment', 'IpayController@directPayment')->name('ipay.direct');
    Route::get('redirect-payment', 'IpayController@redirecPayment')->name('ipay.redirect');
    Route::get('check/{id}', 'IpayController@check')->name('ipay.check');
});


Route::get('ipay/callback', function(Request $request){
    Log::alert('callback');
    Log::debug($request);
});
Route::get('ipay/callback/cancel', function(Request $request){
    Log::alert('cancel');
    Log::debug($request);
});
Route::get('ipay/callback/notif', function(Request $request){
    Log::alert('notif');
    Log::debug($request);
});

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);