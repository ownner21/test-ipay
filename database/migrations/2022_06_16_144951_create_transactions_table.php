<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('product');
            $table->string('qty');
            $table->string('price');
            $table->string('description')->nullable();
            $table->string('referenceId')->nullable();
            $table->string('weight')->nullable();
            $table->string('dimension')->nullable();
            $table->string('buyser')->nullable();
            $table->string('pickup')->nullable();
            $table->string('status')->default('PENDDING');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
