<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 50; $i++){
            DB::table('users')->insert([
                'uuid'=> Str::uuid()->toString(), 
                'name' => $faker->name,
                'job' => $faker->jobTitle,
                'date_of_birt' => $faker->date($format = 'Y-m-d', $max = '2000-01-01')
            ]);
        }
    }
}
