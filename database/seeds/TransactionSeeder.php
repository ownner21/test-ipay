<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = ['Sampo', 'Sabun','Pasta Gigi','Handuk'];
        $price = [1000,2000,3000,4000];
    	for($i = 1; $i <= 50; $i++){
            DB::table('transactions')->insert([
                'product'=> $product[array_rand($product,1)],
                'qty' => rand(1, 3),
                'price' => $price[array_rand($price,1)]
            ]);
        }
        //
    }
}
